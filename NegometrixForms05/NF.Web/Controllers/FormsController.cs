﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NF.Models.Models;
using NF.Service.General.Contracts;
using NF.Service.Services.FormService;
using NF.Service.Services.QuestionService.DocumentQuestionService;
using NF.Service.Services.QuestionService.OptionQuestionService;
using NF.Service.Services.QuestionService.TextQuestionService;
using NF.Web.Mappers;
using NF.Web.Models.AnswerViewModels;
using NF.Web.Models.ViewModels;
using NToastNotify;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace NF.Web.Controllers
{
    public class FormsController : Controller
    {
        private readonly IFormServices formServices;
        private readonly IDocumentQuestionService documentQuestionService;
        private readonly IOptionQuestionService optionQuestionService;
        private readonly IOptionModelService optionModelService;
        private readonly ITextQuestionService textQuestionService;
        private readonly UserManager<User> userManager;
        private readonly IToastNotification toastNotification;
        private readonly IDateTimeProvider dateTimeProvider;

        private readonly IWebHostEnvironment hostingEnvironment;

        public FormsController(IFormServices formServices, UserManager<User> userManager, IToastNotification toastNotification, IDocumentQuestionService documentQuestionService,
            IOptionQuestionService optionQuestionService, IOptionModelService optionModelService, ITextQuestionService textQuestionService, IDateTimeProvider dateTimeProvider, IWebHostEnvironment hostingEnvironment)
        {
            this.formServices = formServices;
            this.userManager = userManager;
            this.documentQuestionService = documentQuestionService;
            this.optionQuestionService = optionQuestionService;
            this.optionModelService = optionModelService;
            this.textQuestionService = textQuestionService;
            this.toastNotification = toastNotification;
            this.dateTimeProvider = dateTimeProvider;
            this.hostingEnvironment = hostingEnvironment;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var form = await formServices.GetQuestionFormsAsync();
            var result = form.FromDto();
            toastNotification.AddSuccessToastMessage("All available forms");
            return View(result);
        }

        [HttpGet]
        public async Task<IActionResult> MyCreatedForms()
        {

            var userName = "";
            if (User != null)
            {
                var user = await userManager.GetUserAsync(User);
                userName = user.UserName;
                //this is for all users even not registered
            }

            var forms = await formServices.GetAllCreatedByUserNameAsync(userName);
            var result = forms.FromDto();

            toastNotification.AddSuccessToastMessage("Showing all forms created by " + userName);

            return View("MyCreatedForms", result);
        }

        [HttpGet]
        public async Task<IActionResult> MyAnsweredForms()
        {
            var userName = "";
            if (User != null)
            {
                var user = await userManager.GetUserAsync(User);
                userName = user.UserName;
            }
            var forms = await formServices.GetAllAnsweredByUserNameAsync(userName);
            var result = forms.FromDto();

            toastNotification.AddSuccessToastMessage("Showing all forms answered by " + userName);

            return View("MyAnsweredForms", result);
        }

        [HttpGet]
        public ActionResult SearchGet()
        {
            return View("SearchGet");
        }

        [HttpGet]
        public async Task<IActionResult> SearchForms(SearchFormVM viewModel)
        {
            var form = await formServices.GetAllByNameAsync(viewModel.Param);
            var result = form.FromDto();
            if (form != null)
            {
                toastNotification.AddSuccessToastMessage("That's what we've found");
            }
            else
            {
                toastNotification.AddSuccessToastMessage("Sorry there are no forms containing that name");
            }
            return View("SearchForms", result);
        }

        [Authorize]
        public IActionResult Create()
        {
            ViewData["create_mode"] = "Create Mode";
            ViewData["form_title"] = "Create Form";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(string state)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = await userManager.GetUserAsync(User);
                    dynamic parsedState = JsonConvert.DeserializeObject(state);
                    var formVM = new FormVM
                    {
                        Name = parsedState.name,
                        Description = parsedState.description,
                        Id = Guid.NewGuid(),
                        CreatorId = user.Id,
                        CreatorUsername = user.UserName,

                        ModifiedOn = dateTimeProvider.GetDateTime()
                    };

                    var formToDTO = formVM.ToDto();
                    await this.formServices.CreateAsync(formToDTO);

                    var questions = parsedState.questions;

                    foreach (var question in parsedState.questions)
                    {
                        if (question.type == "options")
                        {
                            OptionsQuestionVM optionsQuestion = new OptionsQuestionVM
                            {
                                Id = Guid.NewGuid(),
                                Name = question.text,
                                IsRequired = question.required_answer,
                                HasMultipleAnswers = question.multiple_answers,
                                //set fk
                                FormId = formVM.Id
                            };

                            var optionsQuestionDTO = optionsQuestion.ToDto();
                            await this.optionQuestionService.CreateAsync(optionsQuestionDTO);

                            foreach (var option in question.options)
                            {
                                var separateOption = new OptionModelVM
                                {
                                    Id = Guid.NewGuid(),
                                    Text = option,
                                    OptionsQuestionId = optionsQuestion.Id
                                };
                                var optionModelDTO = separateOption.ToDto();
                                await this.optionModelService.CreateAsync(optionModelDTO);
                            }

                        }
                        else if (question.type == "text")
                        {
                            var textQuestion = new TextQuestionVM
                            {
                                Id = Guid.NewGuid(),
                                Name = question.text,
                                IsLongAnswer = question.long_answer,
                                IsRequired = question.required_answer,
                                FormId = formVM.Id
                            };

                            var textQuestionDTO = textQuestion.ToDto();
                            await this.textQuestionService.CreateAsync(textQuestionDTO);
                        }
                        else if (question.type == "document")
                        {
                            var documentQuestion = new DocumentQuestionVM
                            {
                                Id = Guid.NewGuid(),
                                Name = question.text,
                                FormId = formVM.Id,
                                FileNumbers = question.file_number_limit,
                                FileSize = question.file_size_limit
                            };
                            documentQuestion.FormId = formVM.Id;
                            documentQuestion.IsRequired = question.required_answer;

                            var documentQuestionDTO = documentQuestion.ToDto();
                            await this.documentQuestionService.CreateAsync(documentQuestionDTO);
                        }
                    }
                    return RedirectToAction("Index", "Forms"/*, new { area = "" }*/);
                }
                catch (Exception e)
                {
                    return NotFound(e.Message);
                }
            }
            return View();
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Edit(Guid id)
        {
            ViewData["edit_mode"] = "Edit Mode";
            ViewData["form_title"] = "Edit Form";
            var formDTO = await formServices.GetAsync(id);
            var formVM = formDTO.FromDto();
            return View("Edit", formVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string state)
        {
            try
            {
                var user = await userManager.GetUserAsync(User);
                dynamic parsedState = JsonConvert.DeserializeObject(state);
                var formVM = new FormVM
                {
                    Name = parsedState.name,
                    Description = parsedState.description,
                    Id = parsedState.id,
                    CreatorId = user.Id
                };
                //guidsWithQuestions
                var textQuestions = await textQuestionService.GetTextQuestionsGuidsAsync(formVM.Id);
                var optionQuestions = await optionQuestionService.GetOptionQuestionsGuidsAsync(formVM.Id);
                var documentQuestions = await documentQuestionService.GetDocumentQuestionsGuidsAsync(formVM.Id);

                formVM.ModifiedOn = dateTimeProvider.GetDateTime();

                var formToDTO = formVM.ToDto();
                await this.formServices.UpdateAsync(formToDTO);

                var questions = parsedState.questions;

                foreach (var question in parsedState.questions)
                {
                    if (question.type == "options")
                    {
                        var OptionQuestionToDelete = optionQuestions;
                        for (int i = 0; i < OptionQuestionToDelete.Count; i++)
                        {
                            if (question.id == optionQuestions[i])
                            {
                                optionQuestions.Remove(optionQuestions[i]);
                            }
                        }
                        OptionsQuestionVM optionsQuestion = new OptionsQuestionVM
                        {
                            Name = question.text,
                            IsRequired = question.required_answer,
                            HasMultipleAnswers = question.multiple_answers,
                            //set fk
                            FormId = formVM.Id
                        };

                        if (question.id == "")
                        {
                            optionsQuestion.Id = Guid.NewGuid();

                            var optionsQuestionDTOnew = optionsQuestion.ToDto();
                            await this.optionQuestionService.CreateAsync(optionsQuestionDTOnew);
                        }
                        else
                        {
                            optionsQuestion.Id = question.id;

                            var optionsQuestionDTO = optionsQuestion.ToDto();
                            await this.optionQuestionService.UpdateAsync(optionsQuestionDTO);

                        }
                        //list with all options in this question
                        var optionsForOptionQuestion = await optionModelService.GetOptionGuidForQuestionsAsync(optionsQuestion.Id);

                        foreach (var option in question.options)
                        {
                            var optionsForOptionQuestionToDelete = optionsForOptionQuestion;
                            for (int i = 0; i < optionsForOptionQuestionToDelete.Count; i++)
                            {
                                if (option.id == optionsForOptionQuestion[i])
                                {
                                    optionsForOptionQuestion.Remove(optionsForOptionQuestion[i]);

                                }
                            }
                            var separateOption = new OptionModelVM
                            {
                                Text = option.text,
                                OptionsQuestionId = optionsQuestion.Id
                            };
                            if (option.id == "")
                            {
                                option.id = Guid.NewGuid();
                                separateOption.Id = option.id;

                                var optionModelDTOnew = separateOption.ToDto();
                                await this.optionModelService.CreateAsync(optionModelDTOnew);
                            }
                            else
                            {
                                separateOption.Id = option.id;

                                var optionModelDTO = separateOption.ToDto();
                                await this.optionModelService.UpdateAsync(optionModelDTO);
                            }
                        }
                        foreach (var item in optionsForOptionQuestion)
                        {
                            await optionModelService.DeleteAsync(item);
                        }
                    }
                    else if (question.type == "text")
                    {
                        var textQuestionsToDelete = textQuestions;
                        for (int i = 0; i < textQuestionsToDelete.Count; i++)
                        {
                            if (question.id == textQuestions[i])
                            {
                                textQuestions.Remove(textQuestions[i]);
                            }
                        }

                        var textQuestion = new TextQuestionVM
                        {
                            Name = question.text,
                            IsLongAnswer = question.long_answer,
                            IsRequired = question.required_answer,
                            FormId = formVM.Id
                        };

                        if (question.id == "")
                        {
                            question.id = Guid.NewGuid();

                            textQuestion.Id = question.id;

                            var textQuestionDTOnew = textQuestion.ToDto();
                            await this.textQuestionService.CreateAsync(textQuestionDTOnew);
                        }
                        else
                        {
                            textQuestion.Id = question.id;

                            var textQuestionDTO = textQuestion.ToDto();
                            await this.textQuestionService.UpdateAsync(textQuestionDTO);

                        }
                    }
                    else if (question.type == "document")
                    {
                        var documentQuestionsToDelete = documentQuestions;
                        for (int i = 0; i < documentQuestionsToDelete.Count; i++)
                        {
                            if (question.id == documentQuestions[i])
                            {
                                documentQuestions.Remove(documentQuestions[i]);

                            }
                        }

                        var documentQuestion = new DocumentQuestionVM
                        {
                            Name = question.text,
                            FormId = formVM.Id,
                            FileNumbers = question.file_number_limit,
                            FileSize = question.file_size_limit
                        };
                        documentQuestion.FormId = formVM.Id;
                        documentQuestion.IsRequired = question.required_answer;
                        if (question.id == "")
                        {
                            question.id = Guid.NewGuid();
                            documentQuestion.Id = question.id;

                            var documentQuestionDTOnew = documentQuestion.ToDto();
                            await this.documentQuestionService.CreateAsync(documentQuestionDTOnew);
                        }
                        else
                        {
                            documentQuestion.Id = question.id;

                            var documentQuestionDTO = documentQuestion.ToDto();
                            await this.documentQuestionService.UpdateAsync(documentQuestionDTO);
                        }
                    }
                }
                foreach (var item in textQuestions)
                {
                    await textQuestionService.DeleteAsync(item);
                }
                foreach (var item in optionQuestions)
                {
                    await optionQuestionService.DeleteAsync(item);
                }
                foreach (var item in documentQuestions)
                {
                    await documentQuestionService.DeleteAsync(item);
                }

            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                var form = await this.formServices.GetAsync(id);
                var result = form.FromDto();
                return View("DeleteResult", result);
            }
            catch (Exception)
            {
                return View("Index");
            }

        }
        
        public async Task <IActionResult> DeleteResult(Guid id)
        {
            try
            {
                await this.formServices.DeleteAsync(id);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View("Index");
            }
        }

        [HttpGet]
        public async Task<IActionResult> Answer(Guid id)
        {
            var formDTO = await formServices.GetAsync(id);
            var formVM = formDTO.FromDto();

            formVM.TextQuestions.ToList();
            formVM.DocumentQuestions.ToList();
            formVM.OptionsQuestions.ToList();

            return View("AnswerForm", formVM);

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateAnswer(FormVM model)
        {
            var textQ = new TextQuestionAVM
            {
                Name = "Capital of Bulgaria is:",
                TextAnswer = "Sofia",
                IsAnswered = true
            };
            var user = await userManager.GetUserAsync(User);

            var answermodel = new FormAnswerAVM
            {
                Id = new Guid(),
                OptionQuestionAnswers = new List<OptionQuestionAVM>(),
                DocumentQuestionAnswers = new List<DocumentQuestionAVM>(),
                TextQuestionAnswers = new List<TextQuestionAVM>(),
                AnswerDate = DateTime.Today,
                IsAnonymous = model.IsAnonymous,
                FormId = model.Id,
                FormName = model.Name

            };

            answermodel.TextQuestionAnswers.Add(textQ);
            if (user == null)
            {
                answermodel.IsAnonymous = true;
                answermodel.AnseredUserName = "Anonymous";
            }
            else
            {
                answermodel.AnseredUserName = user.UserName;
                answermodel.AnsweredBy = user.Id;
            }
            await this.formServices.CreateAnswerFormAsync(answermodel.ToDto());

            return View("AnsweredFormInfo", answermodel);

        }
        [HttpGet]
        public async Task<ActionResult> CheckOutForm(Guid id)
        {
            try
            {
                var answerModels = await this.formServices.GetAsync(id);
                var model = answerModels.FromDto();
                model.CreatorUsername = this.formServices.GetCreatorUsername(model.CreatorId);
                return View("CheckOutForm", model);
            }
            catch (Exception)
            {
                return View("Index"); ;
            }
        }
        [HttpGet]
        public async Task<ActionResult> CheckOutFormAnswers(Guid id)
        {

            try
            {
                var answerModels = await this.formServices.GetAllAnswersForForm(id);
                var form = await this.formServices.GetAsync(id);
                ViewData["form_title"] = form.Name;
                if (answerModels == null)
                {
                    toastNotification.AddSuccessToastMessage("This form has no answers");

                    return View("Index");
                }
                var result = answerModels.FromDto().ToList();
                return View("CheckOutFormAnswers", result);
            }
            catch (Exception)
            {
                return View("Index");
            }
        }

        public virtual ActionResult PartialAnswer(string id)
        {
            var guiddd = Guid.Parse(id);
            var myModel = this.formServices.GetAnswersForForm(guiddd);
            var result = myModel.FromDto();
            return PartialView("_ViewAnswersForForm", result);
        }

        [HttpPost]
        public ActionResult SendEmails(string form_id, string form_name, string[] emails)
        {
            var success = false;
            string error_msg = "";
            try
            {
                using var message = new MailMessage();
                foreach (var email_addr in emails)
                {
                    message.To.Add(new MailAddress(email_addr));
                }
                message.From = new MailAddress("negometrixform@gmail.com", "Forms Project");
                message.Subject = ($"Форма {form_name}");
                message.IsBodyHtml = true;
                message.Body = ($"https://localhost:5001/Forms/Answer/{form_id}");

                using (var client = new SmtpClient("smtp.gmail.com"))
                {
                    client.Port = 587;
                    client.Credentials = new NetworkCredential("negometrixform@gmail.com", "negometrixForm#1");
                    client.EnableSsl = true;
                    client.Send(message);
                }
                success = true;
            }
            catch (Exception e)
            {
                error_msg = e.Message;
            }

            return Json(new { success = success, error_msg = error_msg, emails = emails });
        }

      
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveAnswer(string json_answer)
        {
            var user = await userManager.GetUserAsync(User);
            dynamic parsedState = JsonConvert.DeserializeObject(json_answer);

            try
            {
                var formAnswerNew = new FormAnswerAVM
                {
                    FormId = parsedState.id,
                    Id = Guid.NewGuid()
                };

                if (user != null)
                {
                    formAnswerNew.AnseredUserName = user.UserName;
                    formAnswerNew.IsAnonymous = false;
                    formAnswerNew.AnsweredBy = user.Id;
                }
                if (user == null)
                {
                    formAnswerNew.IsAnonymous = true;
                    formAnswerNew.AnseredUserName = "Anonymous";
                }
                var formVM = await formServices.GetAsync(formAnswerNew.FormId);
            
                formAnswerNew.FormName = formVM.Name;
               

                foreach (var item in parsedState.text_questions)
                {
                    Guid tempId = item.id;

                    var textQuestionAVM = new TextQuestionAVM()
                    {
                        TextAnswer = item.answer,
                        IsAnswered = true,
                        Name = formVM.TextQuestions.FirstOrDefault(f => f.Id.Equals(tempId)).Name,
                    };
                    formAnswerNew.TextQuestionAnswers.Add(textQuestionAVM);
                }

                foreach (var item in parsedState.option_questions)
                {
                    Guid tempId = item.id;
                    var optionQuestionAVM = new OptionQuestionAVM()
                    {
                        Text = formVM.OptionsQuestions.FirstOrDefault(f => f.Id.Equals(tempId)).Name,
                        IsAnswered = true,

                    };
                    List<Guid> listNewGuidsOptions = new List<Guid>();

                    foreach (var g in item.selected_option_ids)
                    {
                        string guid = g.ToString();
                        Guid tempId2 = new Guid(guid);
                        listNewGuidsOptions.Add(tempId2);
                    }
                    foreach (var option in formVM.OptionsQuestions.FirstOrDefault(o => o.Id == tempId).OptionModels)
                    {
                        var boolChecker = listNewGuidsOptions.Contains(option.Id);
                        optionQuestionAVM.Options.Add(new OptionAVM
                        {
                            IsMarked = boolChecker,
                            Text = option.Text,
                        });
                    }
                    formAnswerNew.OptionQuestionAnswers.Add(optionQuestionAVM);
                }
                if (HttpContext.Request.Form.Files != null)
                {
                    var files = HttpContext.Request.Form.Files;
                    string uniqueDocumentName = null;
                    foreach (var documentQuestion in formVM.DocumentQuestions)
                    {
                        formAnswerNew.DocumentQuestionAnswers.Add(new DocumentQuestionAVM
                        {
                            QuestionText = documentQuestion.Name,
                            FilesCount = documentQuestion.FileNumbers,
                            QuestionId = documentQuestion.Id,
                        });
                    }

                    foreach (var item in files)
                    {
                        string uplpoadsFolder = Path.Combine(hostingEnvironment.WebRootPath, "documents");
                        uniqueDocumentName = Guid.NewGuid().ToString() + "_" + item.FileName;
                        string filePath = Path.Combine(uplpoadsFolder, uniqueDocumentName);
                        await item.CopyToAsync(new FileStream(filePath, FileMode.Create));

                        Guid tempId = new Guid(item.Name);
                        bool isTrue = item.FileName != null;

                        formAnswerNew.DocumentQuestionAnswers.FirstOrDefault(d => d.QuestionId == tempId).FilePaths.Add(new DocumentPathAVM
                        {
                            Path = uniqueDocumentName,
                            IsAnswered = isTrue,
                        });
                    }
                }
                await this.formServices.CreateAnswerFormAsync(formAnswerNew.ToDto());
            }
            catch (Exception e)
            {
                throw e;
            }
            toastNotification.AddSuccessToastMessage("Answer was submitted !");

            return RedirectToAction("Index");
        }
    }
}