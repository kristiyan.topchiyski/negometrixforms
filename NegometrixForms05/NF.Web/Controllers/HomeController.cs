﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NF.Models.Models;
using NF.Service.Services.FormService;
using NF.Web.Mappers;
using NF.Web.Models;
using NF.Web.Models.ViewModels;

namespace NF.Web.Controllers
{
    public class HomeController : Controller
    {
       
        private readonly ILogger<HomeController> _logger;
        private readonly UserManager<User> userManager;
        private readonly IFormServices formServices;
        public HomeController(ILogger<HomeController> logger, IFormServices formServices, UserManager<User> userManager)
        {
            _logger = logger;
            this.formServices = formServices;
            this.userManager = userManager;
        }

        public IActionResult Index()
        {
            List<FormVM> list = new List<FormVM>();

            if (this.formServices.GetMostAnswered() != null)
            {
                list.Add(this.formServices.GetMostAnswered().FromDto());

            }
            if (this.formServices.GetMostRecent() != null)
            {
                list.Add(this.formServices.GetMostRecent().FromDto());

            }

            return View("Index", list);
        }
      
        public IActionResult PageNotFound()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
