﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NF.Web.Models.AnswerViewModels
{
    public class OptionQuestionAVM
    {
        public bool IsAnswered { get; set; }
        public List<OptionAVM> Options { get; set; } = new List<OptionAVM>();
        public string Text { get; set; }

    }
    public class OptionAVM
    {
        public bool IsMarked { get; set; }
        public string Text { get; set; }

    }
}
