﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NF.Web.Models.AnswerViewModels
{
    public class DocumentQuestionAVM
    {
        public Guid QuestionId { get; set; }
        public string QuestionText { get; set; }
        public int FilesCount { get; set; }
        public List<DocumentPathAVM> FilePaths { get; set; } = new List<DocumentPathAVM>();

    }
    
    public class DocumentPathAVM
    {
        public string Path { get; set; }
        public bool IsAnswered { get; set; }
    }
}
