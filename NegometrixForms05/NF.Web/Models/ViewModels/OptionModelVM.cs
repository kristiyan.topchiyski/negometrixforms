﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NF.Web.Models.ViewModels
{
    public class OptionModelVM
    {
        public Guid Id { get; set; }
        public string Text { get; set; }
        public bool IsMarked { get; set; } 
        public OptionsQuestionVM Question { get; set; }
        public Guid OptionsQuestionId { get; set; }
    }
}
