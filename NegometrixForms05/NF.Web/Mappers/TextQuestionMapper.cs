﻿using NF.Models.Models;
using NF.Service.DTOs;
using NF.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NF.Web.Mappers
{
    public static class TextQuestionMapper
    {
        public static TextQuestionDTO ToDto(this TextQuestionVM textQuestion)
        {
            var textQuestionDto = new TextQuestionDTO
            {
                Id = textQuestion.Id,
                Name = textQuestion.Name,
                Text = textQuestion.Text,
                IsLongAnswer = textQuestion.IsLongAnswer,
                IsAnswered = textQuestion.IsAnswered,
                IsRequired = textQuestion.IsRequired,
                FormId = textQuestion.FormId,
               
            };
            return textQuestionDto;
        }

        public static ICollection<TextQuestionDTO> ToDto(this ICollection<TextQuestionVM> textQuestion)
        {
            var textQuestionDto = textQuestion.Select(s => s.ToDto()).ToList();
            return textQuestionDto;
        }

        public static TextQuestionVM FromDto(this TextQuestionDTO textQuestionDto)
        {
            var textQuestion = new TextQuestionVM
            {
                Id = textQuestionDto.Id,
                Name = textQuestionDto.Name,
                Text = textQuestionDto.Text,
                IsLongAnswer = textQuestionDto.IsLongAnswer,
                IsAnswered = textQuestionDto.IsAnswered,
                IsRequired = textQuestionDto.IsRequired,
                FormId=textQuestionDto.FormId,
            };

            return textQuestion;
        }
        public static ICollection<TextQuestionVM> FromDto(this ICollection<TextQuestionDTO> textQuestionsDTO)
        {
            var tQDto = textQuestionsDTO.Select(br => br.FromDto()).ToList();

            return tQDto;
        }
    }
}
