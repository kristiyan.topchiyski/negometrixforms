﻿using NF.Service.Dtos;
using NF.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NF.Web.Mappers
{
    public static class DocumentQuestionMapper
    {
        public static DocumentQuestionDTO ToDto(this DocumentQuestionVM documentQuestionVM)
        {
            var documentQuestionDto = new DocumentQuestionDTO
            {
                Id = documentQuestionVM.Id,
                Name = documentQuestionVM.Name,
                FileNumbers = documentQuestionVM.FileNumbers,
                FormId = documentQuestionVM.FormId,
                FileSize = documentQuestionVM.FileSize,
                IsRequired=documentQuestionVM.IsRequired,

            };
            return documentQuestionDto;
        }

        public static ICollection<DocumentQuestionDTO> ToDto(this ICollection<DocumentQuestionVM> documentQuestionVM)
        {
            var documentQuestionDto = documentQuestionVM.Select(s => s.ToDto()).ToList();
            return documentQuestionDto;
        }

        public static DocumentQuestionVM FromDto(this DocumentQuestionDTO documentQuestionDto)
        {
            var documentQuestion = new DocumentQuestionVM
            {
                Id = documentQuestionDto.Id,
                Name = documentQuestionDto.Name,
                FileNumbers = documentQuestionDto.FileNumbers,
                FormId = documentQuestionDto.FormId,
                FileSize = documentQuestionDto.FileSize,
                IsRequired=documentQuestionDto.IsRequired,

            };

            return documentQuestion;
        }
    }
}
