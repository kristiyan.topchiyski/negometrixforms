
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NF.Data;
using NF.Models.Models.Role;
using NF.Models.Models;
using NF.Service.Services.FormService;
using NF.Service.General.Contracts;
using NF.Service.General;
using NToastNotify;
using NF.Service.Services.QuestionService.TextQuestionService;
using NF.Service.Services.QuestionService.OptionQuestionService;
using NF.Service.Services.QuestionService.DocumentQuestionService;
using NF.Web.Helper;

namespace NegometrixForms
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().AddNToastNotifyToastr(new ToastrOptions()
            {
                ProgressBar = false,
                TimeOut=1500,
                PositionClass = ToastPositions.BottomCenter
            });
            services.AddScoped<IDateTimeProvider, DateTimeProvider>();
            services.AddScoped<IFormServices, FormServices>();
            services.AddScoped<ITextQuestionService, TextQuestionService>();
            services.AddScoped<IOptionQuestionService, OptionQuestionService>();
            services.AddScoped<IOptionModelService, OptionModelService>();
            services.AddScoped<IDocumentQuestionService, DocumentQuestionService>();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 4;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
                
            });

            services.AddDbContext<NFContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<NFContext>()
                .AddDefaultUI()
                .AddDefaultTokenProviders();

            services.AddControllersWithViews();
            services.AddRazorPages();
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseNToastNotify();

            app.UseMiddleware<PageNotFoundMiddleware>();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "areas",
                    pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();

                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
