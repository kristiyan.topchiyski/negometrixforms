﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NF.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NF.Data.Config
{
    internal class OptionModelConfig : IEntityTypeConfiguration<OptionModel>
    {
        public void Configure(EntityTypeBuilder<OptionModel> builder)
        {
            builder.HasKey(o => o.Id);
            builder.HasOne(o => o.Question).WithMany(q => q.OptionModels).HasForeignKey(o => o.OptionsQuestionId);
            builder.Property(o => o.Text);
            builder.Property(o => o.IsMarked);
        }
    }
}
