﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NF.Models.Models;

namespace NF.Data.Config
{
    internal class OptionsQuestionConfig : IEntityTypeConfiguration<OptionsQuestion>
    {
        public void Configure(EntityTypeBuilder<OptionsQuestion> builder)
        {
            builder.HasKey(o => o.Id);
            builder.HasOne(q => q.Form).WithMany(f => f.OptionsQuestions).HasForeignKey(q => q.FormId);
            builder.HasMany(q => q.OptionModels).WithOne(o => o.Question).HasForeignKey(q => q.OptionsQuestionId);
            builder.Property(q => q.HasMultipleAnswers);
            builder.Property(q => q.Name);
            builder.Property(q => q.IsAnswered);
            builder.Property(q => q.IsDeleted);
            builder.Property(q => q.IsRequired);
            builder.Property(q => q.CreatedOn);
            builder.Property(q => q.ModifiedOn);

    }
    }
}
