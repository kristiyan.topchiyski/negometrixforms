﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NF.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NF.Data.Config
{
    internal class TextQuestionConfig : IEntityTypeConfiguration<TextQuestion>
    {
        public void Configure(EntityTypeBuilder<TextQuestion> builder)
        {
            builder.HasKey(t => t.Id);
            builder.HasOne(t => t.Form).WithMany(f => f.TextQuestions).HasForeignKey(t => t.FormId);
            builder.Property(q => q.Name);
            builder.Property(q => q.Text);
            builder.Property(q => q.IsLongAnswer);
            builder.Property(q => q.IsDeleted);
            builder.Property(q => q.IsAnswered);
            builder.Property(q => q.IsRequired);
            builder.Property(q => q.CreatedOn);
            builder.Property(q => q.ModifiedOn);
            builder.Property(q => q.DeletedOn);
        }
    }
}
