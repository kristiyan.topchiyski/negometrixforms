﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NF.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NF.Data.Config
{
    internal class FormConfig : IEntityTypeConfiguration<Form>
    {
        public void Configure(EntityTypeBuilder<Form> builder)
        {
            builder.HasKey(q => q.Id);
            builder.HasMany(f=>f.DocumentQuestions).WithOne(d=>d.Form).HasForeignKey(d => d.FormId);
            builder.HasMany(f => f.OptionsQuestions).WithOne(o => o.Form).HasForeignKey(o => o.FormId);
            builder.HasMany(f => f.TextQuestions).WithOne(t => t.Form).HasForeignKey(t => t.FormId);
            builder.HasOne(f => f.CreatedBy).WithMany(u => u.Forms).HasForeignKey(u => u.CreatorId);
            builder.Property(x => x.IsDeleted).IsRequired();
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.Description).IsRequired();
            builder.Property(x => x.ModifiedOn).IsRequired();
            builder.Property(x => x.CreatedOn).HasDefaultValue(DateTime.Now);
        }
    }
}
