﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NF.Data;
using NF.Service.Dtos;
using NF.Service.General.Contracts;
using NF.Service.Services.FormService;
using System;
using System.Threading.Tasks;

namespace NF.Service.Tests.FormTest
{
    [TestClass]
    public class CreateForm_Should
    {
        [TestMethod]
        public async Task Throw_When_Id_Empty()
        {
            //Arrange

            var options = Utils.GetOptions(nameof(Throw_When_Id_Empty));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);
            //set null value for name
            var moqFormDTO = new Mock<FormDTO>();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new FormServices(assertContext, mockDateTime.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateAsync(moqFormDTO.Object));
            }
        }
        [TestMethod]
        public async Task Test_If_Create_Is_Putting_Data_In_DataBase_And_Returns_Correct_Value()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Test_If_Create_Is_Putting_Data_In_DataBase_And_Returns_Correct_Value));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            Guid testGuid = new Guid("63FA647C-AD54-4BCC-A860-E5A2664B029D");

            var moqFormDTO = new Mock<FormDTO>();

            moqFormDTO.Object.Id = testGuid;
            moqFormDTO.Object.Name = "Test";

            var toDtoStyle = moqFormDTO.Object;

            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new FormServices(assertContext, mockDateTime.Object);
                var resultOQDTO = await sut.CreateAsync(moqFormDTO.Object);

                Assert.AreEqual(resultOQDTO.Id, moqFormDTO.Object.Id);
                Assert.AreEqual(resultOQDTO.Name, moqFormDTO.Object.Name);
            }
        }
    }
}
