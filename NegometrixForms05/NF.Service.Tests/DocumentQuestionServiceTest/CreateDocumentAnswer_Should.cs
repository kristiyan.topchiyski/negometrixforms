﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NF.Data;
using NF.Service.Dtos;
using NF.Service.General.Contracts;
using NF.Service.Services.QuestionService.DocumentQuestionService;
using System;
using System.Threading.Tasks;
namespace NF.Service.Tests.DocumentQuestionServiceTest
{
    [TestClass]
    public class CreateDocumentAnswer_Should
    {
        [TestMethod]
        public async Task Throw_When_Id_Empty()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_Id_Empty));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);
            //set null value for name
            var moqDocumentQuestionDTO = new Mock<DocumentQuestionDTO>();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new DocumentQuestionService(assertContext, mockDateTime.Object);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateAsync(moqDocumentQuestionDTO.Object));
            }
        }

        [TestMethod]
        public async Task Test_If_Create_Is_Putting_Data_In_DataBase_And_Returns_Correct_Value()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Test_If_Create_Is_Putting_Data_In_DataBase_And_Returns_Correct_Value));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            Guid testGuid = new Guid("62FA647C-AD54-4BCC-A860-E5A2664B029D");

            var documentQuestion = new Mock<DocumentQuestionDTO>();

            documentQuestion.Object.Id = testGuid;
            documentQuestion.Object.Name = "Test";

            var toDtoStyle = documentQuestion.Object;
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new DocumentQuestionService(assertContext, mockDateTime.Object);
                var resultOQDTO = await sut.CreateAsync(documentQuestion.Object);

                Assert.AreEqual(resultOQDTO.Id, documentQuestion.Object.Id);
                Assert.AreEqual(resultOQDTO.Name, documentQuestion.Object.Name);
            }
        }
    }
}
