﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NF.Data;
using NF.Service.Dtos;
using NF.Service.General.Contracts;
using NF.Service.Mappers;
using NF.Service.Services.QuestionService.DocumentQuestionService;
using System;
using System.Threading.Tasks;


namespace NF.Service.Tests.DocumentQuestionServiceTest
{
    [TestClass]
    public class UpdateDocumentQuestion_Should
    {
        [TestMethod]
        public async Task Throw_When_Id_Empty()
        {
            //Arrange

            var options = Utils.GetOptions(nameof(Throw_When_Id_Empty));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);
        
            var moqDocumentQuestionDTO = new Mock<DocumentQuestionDTO>();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new DocumentQuestionService(assertContext, mockDateTime.Object);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateAsync(moqDocumentQuestionDTO.Object));
            }
        }
        [TestMethod]
        public async Task Update_Document_Answer()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Update_Document_Answer));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            var moqDocumentQuestionDTO = new Mock<DocumentQuestionDTO>();
            moqDocumentQuestionDTO.Object.Id = Guid.NewGuid();
            moqDocumentQuestionDTO.Object.FileSize = 1;
            moqDocumentQuestionDTO.Object.FileNumbers = 1;
            moqDocumentQuestionDTO.Object.IsRequired = true;
            moqDocumentQuestionDTO.Object.Name = "Name";

            var documentQuestion = moqDocumentQuestionDTO.Object.FromDto();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new DocumentQuestionService(assertContext, mockDateTime.Object);
                //add to mock database
                await assertContext.DocumentQuestions.AddAsync(documentQuestion);
                await assertContext.SaveChangesAsync();

                var result = await sut.UpdateAsync(moqDocumentQuestionDTO.Object);
                var actual = assertContext.DocumentQuestions.FindAsync(moqDocumentQuestionDTO.Object.Id).Result;

                Assert.AreEqual(result.Id, actual.Id);
                Assert.AreEqual(result.FileNumbers, actual.FileNumbers);
                Assert.AreEqual(result.FileSize, actual.FileSize);
                Assert.AreEqual(result.IsRequired, actual.IsRequired);
                Assert.AreEqual(result.Name, actual.Name);
            }
        }
        [TestMethod]
        public async Task Throw_When_This_Question_IsNotIn_Database()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_This_Question_IsNotIn_Database));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            var moqDocumentQuestionDTO = new Mock<DocumentQuestionDTO>();
            moqDocumentQuestionDTO.Object.Id = Guid.NewGuid();

            var textQuestion = moqDocumentQuestionDTO.Object.FromDto();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new DocumentQuestionService(assertContext, mockDateTime.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateAsync(moqDocumentQuestionDTO.Object));

            }
        }
    }
}
