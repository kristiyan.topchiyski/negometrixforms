﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NF.Data;
using NF.Service.DTOs;
using NF.Service.General.Contracts;
using NF.Service.Mappers;
using NF.Service.Services.QuestionService.TextQuestionService;
using System;
using System.Threading.Tasks;

namespace NF.Service.Tests.TextQuestionServiceTest
{
    [TestClass]
    public class DeleteTextQuestion_Should
    {
        [TestMethod]
        public async Task Delete_Text_Question()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Delete_Text_Question));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            var moqTextQuestionDTO = new Mock<TextQuestionDTO>();
            moqTextQuestionDTO.Object.Id = Guid.NewGuid();
            moqTextQuestionDTO.Object.IsLongAnswer = true;
            moqTextQuestionDTO.Object.IsRequired = true;
            moqTextQuestionDTO.Object.Name = "Name";

            var textQuestion = moqTextQuestionDTO.Object.FromDto();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new TextQuestionService(assertContext, mockDateTime.Object);
                //add to mock database
                await assertContext.TextQuestions.AddAsync(textQuestion);
                await assertContext.SaveChangesAsync();

                var result = await sut.DeleteAsync(moqTextQuestionDTO.Object.Id);

                Assert.AreEqual(result, textQuestion.Name);
            }
        }
        [TestMethod]
        public async Task Throw_When_Id_Empty()
        {
            //Arrange

            var options = Utils.GetOptions(nameof(Throw_When_Id_Empty));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);
            //set null value for name
            var moqTextQuestionDTO = new Mock<TextQuestionDTO>();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new TextQuestionService(assertContext, mockDateTime.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DeleteAsync(moqTextQuestionDTO.Object.Id));
            }
        }
        [TestMethod]
        public async Task Throw_When_This_Question_IsNotIn_Database()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_This_Question_IsNotIn_Database));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            var moqTextQuestionDTO = new Mock<TextQuestionDTO>();
            moqTextQuestionDTO.Object.Id = Guid.NewGuid();

            var textQuestion = moqTextQuestionDTO.Object.FromDto();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new TextQuestionService(assertContext, mockDateTime.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DeleteAsync(moqTextQuestionDTO.Object.Id));
            }
        }
    }
}