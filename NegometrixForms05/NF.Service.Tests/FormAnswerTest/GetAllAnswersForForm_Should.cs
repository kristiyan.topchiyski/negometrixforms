﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NF.Data;
using NF.Service.Dtos;
using NF.Service.General.Contracts;
using NF.Service.Services.FormService;
using System;
using System.Threading.Tasks;

namespace NF.Service.Tests.FormAnswerTest
{
    [TestClass]
    public class GetAllAnswersForForm_Should
    {
        [TestMethod]
        public void Throw_When_Empty()
        {
            //Arrange

            var options = Utils.GetOptions(nameof(Throw_When_Empty));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);
           
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new FormServices(assertContext, mockDateTime.Object);
                var item = sut.GetMostAnswered();
                Assert.IsNull(item);
            }
        }

        [TestMethod]
        public async Task Return_Correct_DTO()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Return_Correct_DTO));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            Guid testGuid = new Guid("62FA647C-AD54-4BCC-A860-E5A2664B029D");

            var moqFormDTO = new Mock<FormDTO>();

            moqFormDTO.Object.Id = testGuid;
            moqFormDTO.Object.Name = "Test";
            

            var toDtoStyle = moqFormDTO.Object;

            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new FormServices(assertContext, mockDateTime.Object);
                var resultOQDTO = await sut.CreateAsync(moqFormDTO.Object);

                Assert.AreEqual(resultOQDTO.Name, sut.GetMostRecent().Name);
            }
        }
    }
}
