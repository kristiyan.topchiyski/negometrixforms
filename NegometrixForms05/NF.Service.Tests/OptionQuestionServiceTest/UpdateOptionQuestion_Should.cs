﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NF.Data;
using NF.Service.DTOs;
using NF.Service.General.Contracts;
using NF.Service.Mappers;
using NF.Service.Services.QuestionService.OptionQuestionService;
using System;
using System.Threading.Tasks;

namespace NF.Service.Tests.OptionQuestionServiceTest
{
    [TestClass]
    public class UpdateOptionQuestion_Should
    {
        [TestMethod]
        public async Task Throw_When_Id_Empty()
        {
            //Arrange

            var options = Utils.GetOptions(nameof(Throw_When_Id_Empty));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);
            //set null value for name
            var moqOptionQuestionDTO = new Mock<OptionsQuestionDTO>();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new OptionQuestionService(assertContext, mockDateTime.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateAsync(moqOptionQuestionDTO.Object));
            }

        }
        [TestMethod]
        public async Task Update_Option_Question()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Update_Option_Question));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            var moqOptionQuestionDTO = new Mock<OptionsQuestionDTO>();
            moqOptionQuestionDTO.Object.Id = Guid.NewGuid();
            moqOptionQuestionDTO.Object.HasMultipleAnswers = true;
            moqOptionQuestionDTO.Object.IsRequired = true;
            moqOptionQuestionDTO.Object.Name = "Name";

            var optionQuestion = moqOptionQuestionDTO.Object.FromDto();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new OptionQuestionService(assertContext, mockDateTime.Object);
                //add to mock database
                await assertContext.OptionQuestions.AddAsync(optionQuestion);
                await assertContext.SaveChangesAsync();

                var result = await sut.UpdateAsync(moqOptionQuestionDTO.Object);
                var actual = assertContext.OptionQuestions.FindAsync(moqOptionQuestionDTO.Object.Id).Result;

                Assert.AreEqual(result.Id, actual.Id);
                Assert.AreEqual(result.HasMultipleAnswers, actual.HasMultipleAnswers);
                Assert.AreEqual(result.IsRequired, actual.IsRequired);
                Assert.AreEqual(result.Name, actual.Name);
            }
        }
        [TestMethod]
        public async Task Throw_When_This_Question_IsNotIn_Database()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_This_Question_IsNotIn_Database));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            var moqOptionQuestionDTO = new Mock<OptionsQuestionDTO>();
            moqOptionQuestionDTO.Object.Id = Guid.NewGuid();

            var textQuestion = moqOptionQuestionDTO.Object.FromDto();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new OptionQuestionService(assertContext, mockDateTime.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateAsync(moqOptionQuestionDTO.Object));

            }
        }
    }
}
