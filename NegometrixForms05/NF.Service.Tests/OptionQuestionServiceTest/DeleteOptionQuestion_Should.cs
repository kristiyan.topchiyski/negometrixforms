﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NF.Data;
using NF.Service.DTOs;
using NF.Service.General.Contracts;
using NF.Service.Mappers;
using NF.Service.Services.QuestionService.OptionQuestionService;
using System;
using System.Threading.Tasks;

namespace NF.Service.Tests.OptionQuestionServiceTest
{
    [TestClass]
    public class DeleteOptionQuestion_Should
    {
        [TestMethod]
        public async Task Throw_When_Id_Empty()
        {
            //Arrange

            var options = Utils.GetOptions(nameof(Throw_When_Id_Empty));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);
            //set null value for name
            var moqOptionQuestionDTO = new Mock<OptionsQuestionDTO>();
            //Act & Assert
            using var assertContext = new NFContext(options);
            var sut = new OptionQuestionService(assertContext, mockDateTime.Object);
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DeleteAsync(moqOptionQuestionDTO.Object.Id));
        }
        [TestMethod]
        public async Task Return_Proper_String_Name_Of_Deleted()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Return_Proper_String_Name_Of_Deleted));

            Guid tempGuid = new Guid("62FA647C-AD54-4BCC-A860-E5A2664B029D");
            var mockDateTime = new Mock<IDateTimeProvider>();

            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            var optionsQuestionDTO = new Mock<OptionsQuestionDTO>();
            optionsQuestionDTO.Object.Id = tempGuid;
            optionsQuestionDTO.Object.Name = "Test";
            optionsQuestionDTO.Object.IsDeleted = false;
            var optionq = optionsQuestionDTO.Object.FromDto();
            using (var arrangeContext = new NFContext(options))
            {
                arrangeContext.OptionQuestions.Add(optionq);
                arrangeContext.SaveChanges();
            }
            //Act & Assert
            using var assertContext = new NFContext(options);
            var sut = new OptionQuestionService(assertContext, mockDateTime.Object);
            var result = await sut.DeleteAsync(tempGuid);
            Assert.AreEqual(result, optionsQuestionDTO.Object.Name);

        }
        [TestMethod]
        public async Task Throw_When_This_Question_IsNotIn_Database()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_This_Question_IsNotIn_Database));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            var moqOptionQuestionDTO = new Mock<OptionsQuestionDTO>();
            moqOptionQuestionDTO.Object.Id = Guid.NewGuid();

            var textQuestion = moqOptionQuestionDTO.Object.FromDto();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new OptionQuestionService(assertContext, mockDateTime.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DeleteAsync(moqOptionQuestionDTO.Object.Id));

            }
        }
    }
}
