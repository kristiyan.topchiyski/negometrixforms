﻿using NF.Models.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;


public class DocumentPath
{
    [Key]
    public Guid Id { get; set; }
    public string Path { get; set; }
    public Guid DocumentQuestionId { get; set; }
    public DocumentQuestion DocumentQuestion { get; set; }

}

