﻿using NF.Models.Entity;
using NF.Models.Models.FormModel.QuestionModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace NF.Models.Models
{
    public class Form : IEntityDb
    {
        
        //PK
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        public User CreatedBy { get; set; }
        public Guid CreatorId { get; set; }

        public ICollection<FormAnswers> FormAnswers { get; set; }
        public int AnswersCount { get; set; }
        public ICollection<DocumentQuestion> DocumentQuestions { get; set; }
        public ICollection<OptionsQuestion> OptionsQuestions { get; set; }
        public ICollection<TextQuestion> TextQuestions { get; set; }
        //new 
        public bool IsDeleted { get; set; } = false;
        
        //Entity-s
        public DateTime ModifiedOn { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? DeletedOn { get; set; }

    }
}
