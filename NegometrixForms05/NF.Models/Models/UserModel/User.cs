﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Text;

namespace NF.Models.Models
{
    public class User : IdentityUser<Guid>
    {

        public bool IsDeleted { get; set; }
        public bool IsBanned { get; set; }
        public string BanReason { get; set; }
        public ICollection<Form> Forms { get; set; }
        public Guid FormsId { get; set; }
    }
}
