﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NF.Models.Entity
{
    public interface IEntityQuestions
    {
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsAnswered { get; set; }
        public bool IsRequired { get; set; }
    }
}
