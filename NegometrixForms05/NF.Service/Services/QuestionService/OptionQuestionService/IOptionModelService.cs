﻿using NF.Service.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NF.Service.Services.QuestionService.OptionQuestionService
{
    public interface IOptionModelService
    {
        public Task<bool> CreateAsync(OptionModelDTO model);
        public Task<OptionModelDTO> UpdateAsync(OptionModelDTO model);
        Task<List<Guid>> GetOptionGuidForQuestionsAsync(Guid id);
        Task<string> DeleteAsync(Guid id);

    }
}
