﻿using NF.Service.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NF.Service.Services.QuestionService.TextQuestionService
{
    public interface ITextQuestionService
    {
        Task<TextQuestionDTO> CreateAsync(TextQuestionDTO textQuestionDTO);
        Task<TextQuestionDTO> UpdateAsync(TextQuestionDTO model);
        Task<string> DeleteAsync(Guid id);
        Task<List<Guid>> GetTextQuestionsGuidsAsync(Guid id);
    }
}
