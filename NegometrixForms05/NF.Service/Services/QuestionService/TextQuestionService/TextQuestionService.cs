﻿using Microsoft.EntityFrameworkCore;
using NF.Data;
using NF.Service.DTOs;
using NF.Service.General.Contracts;
using NF.Service.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NF.Service.Services.QuestionService.TextQuestionService
{
    public class TextQuestionService : ITextQuestionService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly NFContext context;

        public TextQuestionService(NFContext context, IDateTimeProvider dateTimeProvider)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(context));

        }

        public async Task<TextQuestionDTO> CreateAsync(TextQuestionDTO textQuestionDTO)
        {
            textQuestionDTO.Id.IsEmpty();
            var tq = textQuestionDTO.FromDto();
            tq.CreatedOn = this.dateTimeProvider.GetDateTime();
            await this.context.TextQuestions.AddAsync(tq);
            await this.context.SaveChangesAsync();
            return textQuestionDTO;
        }
        public async Task<TextQuestionDTO> UpdateAsync(TextQuestionDTO model)
        {
            model.Id.IsEmpty();
            var toBeUpdated = await this.context.TextQuestions
                .FirstOrDefaultAsync(x => x.Id == model.Id && !x.IsDeleted);
            toBeUpdated.CheckForNull(Validator.nullMsg);

            toBeUpdated.ModifiedOn = this.dateTimeProvider.GetDateTime();
            toBeUpdated.Name = model.Name;
            toBeUpdated.IsLongAnswer = model.IsLongAnswer;
            toBeUpdated.IsRequired = model.IsRequired;
            
            this.context.Update(toBeUpdated);
            await this.context.SaveChangesAsync();
            //
            return model;
        }
        public async Task<List<Guid>> GetTextQuestionsGuidsAsync(Guid id)
        {
            id.IsEmpty();
            List<Guid> listGuids = new List<Guid>();
            var textQuestionsForForm = await this.context.TextQuestions.Where(i => i.FormId == id).ToListAsync();
            
            foreach (var item in textQuestionsForForm)
            {
                listGuids.Add(item.Id);
            }        
            return listGuids;
        }

        public async Task<string> DeleteAsync(Guid id)
        {
            id.IsEmpty();

            var textQuestion = await this.context.TextQuestions
                .Where(q => q.IsDeleted == false)
                .FirstOrDefaultAsync(q => q.Id == id);

            textQuestion.CheckForNull(Validator.nullMsg);
            textQuestion.IsDeleted = true;
            textQuestion.DeletedOn = this.dateTimeProvider.GetDateTime();
            this.context.Remove(textQuestion);
            await this.context.SaveChangesAsync();
            return textQuestion.Name;
        }
    }
}
