﻿using Microsoft.EntityFrameworkCore;
using NF.Data;
using NF.Service.Dtos;
using NF.Service.General.Contracts;
using NF.Service.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NF.Service.Services.QuestionService.DocumentQuestionService
{
    public class DocumentQuestionService : IDocumentQuestionService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly NFContext context;

        public DocumentQuestionService(NFContext context, IDateTimeProvider dateTimeProvider)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(context));
        }
        public  Task<bool> AddDocumentAsync(DocumentPathDTO docPath, Guid questionId)
        {
            throw new NotImplementedException();
        }

        public async Task<DocumentQuestionDTO> CreateAsync(DocumentQuestionDTO documentQuestionDTO)
        {
            documentQuestionDTO.CheckForNull(Validator.nullMsg);
            documentQuestionDTO.Id.IsEmpty(Validator.nullMsg);

            var dq = documentQuestionDTO.FromDto();
            dq.CreatedOn = this.dateTimeProvider.GetDateTime();

            await this.context.DocumentQuestions.AddAsync(dq);
            await this.context.SaveChangesAsync();
            return documentQuestionDTO;
        }

        public async Task<string> DeleteAsync(Guid id)
        {
            id.IsEmpty();

            var documentQuestion = await this.context.DocumentQuestions
                .Where(q => q.IsDeleted == false)
                .FirstOrDefaultAsync(q => q.Id == id);

            documentQuestion.CheckForNull(Validator.nullMsg);
            documentQuestion.IsDeleted = true;
            documentQuestion.DeletedOn = this.dateTimeProvider.GetDateTime();
            this.context.Remove(documentQuestion);
            await this.context.SaveChangesAsync();
            return documentQuestion.Name;
        }

        public async Task<DocumentQuestionDTO> UpdateAsync(DocumentQuestionDTO model)
        {
            model.Id.IsEmpty();
            model.CheckForNull(Validator.nullMsg);
            var toBeUpdated = await this.context.DocumentQuestions  
              .FirstOrDefaultAsync(x => x.Id == model.Id && !x.IsDeleted);
            toBeUpdated.CheckForNull(Validator.nullMsg);
            toBeUpdated.ModifiedOn = this.dateTimeProvider.GetDateTime();
            toBeUpdated.Name = model.Name;
            toBeUpdated.IsRequired = model.IsRequired;
            toBeUpdated.FileNumbers = model.FileNumbers;
            toBeUpdated.FileSize = model.FileSize;

            this.context.Update(toBeUpdated);
            await this.context.SaveChangesAsync();
            return model;
        }
        public async Task<List<Guid>> GetDocumentQuestionsGuidsAsync(Guid id)
        {
            id.IsEmpty();
            List<Guid> listGuids = new List<Guid>();
            var documentQuestionsForForm = await this.context.DocumentQuestions.Where(i => i.FormId == id).ToListAsync();
            foreach (var item in documentQuestionsForForm)
            {
                listGuids.Add(item.Id);
            }
            return listGuids;
        }
    }
}
