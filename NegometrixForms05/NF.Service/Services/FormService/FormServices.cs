﻿using Microsoft.EntityFrameworkCore;
using MoreLinq;
using NF.Data;
using NF.Models.Models;
using NF.Service.Dtos;
using NF.Service.General.Contracts;
using NF.Service.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace NF.Service.Services.FormService
{
    public class FormServices : IFormServices
    {
        private readonly NFContext context;
        private readonly IDateTimeProvider dateTimeProvider;

        public FormServices(NFContext context, IDateTimeProvider dateTimeProvider)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context), Validator.nullMsg);
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider), Validator.nullMsg);
        }
      
        public async Task<FormDTO> GetAsync(Guid id)
        {
            id.IsEmpty();

            var questionForm = await this.context.Forms.Include(q => q.TextQuestions)
                                                .Include(q => q.OptionsQuestions)
                                                .ThenInclude(o => o.OptionModels)
                                                .Include(q => q.DocumentQuestions)
                                                .FirstOrDefaultAsync(q => q.Id == id && !q.IsDeleted);
            questionForm.CheckForNull(Validator.nullMsg);

            var questionFormDTO = questionForm.ToDto();
            return questionFormDTO;
        }

        public async Task<ICollection<FormDTO>> GetQuestionFormsAsync()
        {
            var questionFormsList = await this.context.Forms.Include(q => q.TextQuestions)
                                                .Include(q => q.OptionsQuestions)
                                                .Include(q => q.DocumentQuestions)
                                                .Where(b => b.IsDeleted == false)
                                                .ToListAsync();

            var questionFormDTO = questionFormsList.ToDto();

            return questionFormDTO;
        }

        public async Task<FormDTO> CreateAsync(FormDTO questionFormDTO)
        {
            questionFormDTO.Id.IsEmpty();
            questionFormDTO.CheckForNull(Validator.nullMsg);

            var newQuestionForm = questionFormDTO.FromDto();
            newQuestionForm.CreatedOn = this.dateTimeProvider.GetDateTime();
            await this.context.Forms.AddAsync(newQuestionForm);
            await this.context.SaveChangesAsync();

            return questionFormDTO;
        }

        public async Task<string> DeleteAsync(Guid id)
        {
            id.IsEmpty();
            Form toBeDeleted;

            toBeDeleted = await this.context.Forms.Include(q => q.TextQuestions)
                                            .Include(q => q.OptionsQuestions)
                                            .ThenInclude(x => x.OptionModels)
                                            .Include(q => q.DocumentQuestions)
                                            .ThenInclude(p => p.Paths)
                                            .FirstOrDefaultAsync(b => b.Id == id);

            toBeDeleted.CheckForNull(Validator.nullMsg);

            this.context.Forms.Remove(toBeDeleted);
            await this.context.SaveChangesAsync();

            return toBeDeleted.Name;
        }

        public async Task<FormDTO> UpdateAsync(FormDTO questionFormDTO)
        {
            questionFormDTO.Id.IsEmpty();
            var form = questionFormDTO.FromDto();
            this.context.Update(form);
            await this.context.SaveChangesAsync();

            return questionFormDTO;
        }

        public async Task<ICollection<FormDTO>> GetAllCreatedByUserNameAsync(string userName)
        {
            userName.EmptyString();
            var questionFormsList = await this.context.Forms.Include(q => q.TextQuestions)
                                                 .Include(q => q.OptionsQuestions)
                                                 .Include(q => q.DocumentQuestions)
                                                 .Include(q => q.CreatedBy)

                                                 .Where(b => b.IsDeleted == false)
                                                 .Where(b => b.CreatedBy.UserName == userName)
                                                 .ToListAsync();

            var questionFormDTO = questionFormsList.ToDto();

            return questionFormDTO;
        }
        public async Task<ICollection<FormDTO>> GetAllByNameAsync(string name)
        {
            name.EmptyString();
            var questionFormsList = await this.context.Forms.Include(q => q.TextQuestions)
                                                 .Include(q => q.OptionsQuestions)
                                                 .Include(q => q.DocumentQuestions)
                                                 .Where(b => b.IsDeleted == false)
                                                 .Where(b => b.Name.Contains(name) || b.Name.Equals(name))
                                                 .ToListAsync();

            var questionFormDTO = questionFormsList.ToDto();

            return questionFormDTO;
        }
        public async Task<ICollection<FormAnswerDTO>> GetAllAnsweredByUserNameAsync(string userName)
        {
            var questionFormsList = await this.context.FormAnswers
                                               .Where(b => b.IsDeleted == false)
                                               .Where(b => b.AnsweredBy == context.Users.FirstOrDefault(x => x.UserName == userName).Id)
                                               .ToListAsync();

            var questionFormDTO = questionFormsList.ToDto();

            return questionFormDTO;
        }
        public async Task<ICollection<FormAnswerDTO>> GetAllAnswersForForm(Guid formId)
        {
            var questionFormsList = await this.context.FormAnswers
                                               .Where(b => b.IsDeleted == false)
                                               .Where(b => b.FormId == formId)
                                               .ToListAsync();

            var formAnswerDTO = questionFormsList.ToDto();

            return formAnswerDTO;
        }
        public FormAnswerDTO GetAnswersForForm(Guid asnwerId)
        {
            var questionFormsList = this.context.FormAnswers
                                               .Where(b => b.IsDeleted == false)
                                               .FirstOrDefault(b => b.Id == asnwerId);

            var formAnswerDTO = questionFormsList.ToDto();

            return formAnswerDTO;
        }

        public async Task<FormAnswerDTO> CreateAnswerFormAsync(FormAnswerDTO model, Guid AnswerUserId)
        {
            model.CheckForNull(Validator.nullMsg);

            var newQuestionForm = model.FromDto();
            newQuestionForm.AnswerDate = this.dateTimeProvider.GetDateTime().Date;
            newQuestionForm.AnsweredBy = AnswerUserId;
            await this.context.FormAnswers.AddAsync(newQuestionForm);
            await this.context.SaveChangesAsync();

            return model;
        }

        public async Task<FormAnswerDTO> CreateAnswerFormAsync(FormAnswerDTO model)
        {
            model.CheckForNull(Validator.nullMsg);

            var newQuestionForm = model.FromDto();
            if (model.AnseredUserName == null)
            {
                newQuestionForm.IsAnonymous = true;
            }
            this.context.Forms.FirstOrDefault(x => x.Id == model.FormId).AnswersCount++;
            newQuestionForm.AnswerDate = this.dateTimeProvider.GetDateTime().Date;
            await this.context.FormAnswers.AddAsync(newQuestionForm);
            await this.context.SaveChangesAsync();

            return model;
        }

        public async Task<bool> DeleteFormAnswer(Guid asnwerId)
        {
            asnwerId.IsEmpty();

            var toBeDeleted = this.context.FormAnswers.FirstOrDefault(q => q.Id == asnwerId && q.IsDeleted == false);
            toBeDeleted.CheckForNull(Validator.nullMsg);

            toBeDeleted.IsDeleted = true;
            toBeDeleted.DeletedOn = this.dateTimeProvider.GetDateTime().Date;

            this.context.Update(toBeDeleted);
            await this.context.SaveChangesAsync();

            return true;
        }

        public string GetCreatorUsername(Guid creatorId)
        {
            creatorId.IsEmpty();
            var name = this.context.Users.FirstOrDefault(x => x.Id == creatorId).UserName;
            return name;
        }

        public FormDTO GetMostAnswered()
        {
            try
            {
                var maxObject = this.context.Forms.OrderByDescending(item => item.AnswersCount).First() ?? null;
                return maxObject.ToDto();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public FormDTO GetMostRecent()
        {
            try
            {
                var maxObject = this.context.Forms.OrderByDescending(item => item.CreatedOn).First() ?? null;
                return maxObject.ToDto();
            }
            catch (Exception)
            {

                return null;
            }
        }
    }
}
