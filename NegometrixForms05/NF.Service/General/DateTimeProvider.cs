﻿using NF.Service.General.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace NF.Service.General
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime GetDateTime()
        {
            return DateTime.UtcNow;
        }
    }
}
