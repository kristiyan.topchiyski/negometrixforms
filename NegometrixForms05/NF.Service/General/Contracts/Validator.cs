﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NF.Service.General.Contracts
{
    public static class Validator
    {
        public const string nullMsg = "This object cannot be null.";
        public const string guidEmptyMsg = "ID cannot be empty";
        public const string stringEmptyMsg = "String cannot be empty";
        public static void CheckForNull<T>(this T obj, string msg)
        {
            if (obj == null)
            {
                string name = typeof(T).Name;
                throw new ArgumentNullException(name, msg);
            }
        }
        public static void CheckForListCount<T>(this List<T> list, string msg)
        {
            if (list.Count == 0)
            {
                string name = typeof(T).Name;
                throw new ArgumentOutOfRangeException(name, msg);
            }
        }
        public static void IsEmpty(this Guid id, string msg = guidEmptyMsg)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentNullException("id", msg);

            }
        }
        public static void EmptyString(this string emptyString, string msg = stringEmptyMsg)
        {
            if (emptyString == string.Empty)
            {
                throw new ArgumentNullException(emptyString, msg);

            }
        }
    }
}
