﻿using Microsoft.VisualBasic.CompilerServices;
using NF.Models.Models;
using NF.Service.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NF.Service.Mappers
{
    public static class OptionModelMapper
    {
        public static OptionModelDTO ToDto(this OptionModel optionmodel)
        {
            var optionModelDto = new OptionModelDTO
            {
                Id = optionmodel.Id,
                OptionsQuestionId= optionmodel.OptionsQuestionId,
                Text= optionmodel.Text,
                IsMarked= optionmodel.IsMarked,
                Question= optionmodel.Question,

            };
            return optionModelDto;
        }

        public static ICollection<OptionModelDTO> ToDto(this ICollection<OptionModel> optionModel)
        {
            var optionModelDto = optionModel?.Select(s => s.ToDto()).ToList();
            return optionModelDto;
        }

        public static OptionModel FromDto(this OptionModelDTO optionmodelDTO)
        {
            var optionModel = new OptionModel
            {
                Id = optionmodelDTO.Id,
                OptionsQuestionId = optionmodelDTO.OptionsQuestionId,
                Text = optionmodelDTO.Text,
                IsMarked = optionmodelDTO.IsMarked,
                Question = optionmodelDTO.Question,
                

            };

            return optionModel;
        }
        public static ICollection<OptionModel> FromDto(this ICollection<OptionModelDTO> optionModel)
        {
            var optionModelDto = optionModel?.Select(FromDto).ToList();
            //var optionModelDto = optionModel?.Select(s=>s.FromDto()).ToList();
            return optionModelDto;
        }

    }
}
