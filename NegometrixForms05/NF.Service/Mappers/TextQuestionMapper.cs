﻿using NF.Models.Models;
using NF.Service.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NF.Service.Mappers
{
    public static class TextQuestionMapper
    {
        public static TextQuestionDTO ToDto(this TextQuestion textQuestion)
        {
            var textQuestionDto = new TextQuestionDTO
            {
                Id = textQuestion.Id,
                Name = textQuestion.Name,
                Text = textQuestion.Text,
                IsLongAnswer = textQuestion.IsLongAnswer,
                IsAnswered = textQuestion.IsAnswered,
                IsDeleted = textQuestion.IsDeleted,
                FormId=textQuestion.FormId,
                IsRequired=textQuestion.IsRequired,
                
            };
            return textQuestionDto;
        }

        public static ICollection<TextQuestionDTO> ToDto(this ICollection<TextQuestion> textQuestion)
        {
            var textQuestionDto = textQuestion.Select(s => s.ToDto()).ToList();
            return textQuestionDto;
        }

        public static TextQuestion FromDto(this TextQuestionDTO textQuestionDto)
        {
            var textQuestion = new TextQuestion
            {
                Id = textQuestionDto.Id,
                Name = textQuestionDto.Name,
                Text = textQuestionDto.Text,
                IsLongAnswer = textQuestionDto.IsLongAnswer,
                IsAnswered = textQuestionDto.IsAnswered,
                IsDeleted = textQuestionDto.IsDeleted,
                IsRequired=textQuestionDto.IsRequired,
                FormId=textQuestionDto.FormId,
            };

            return textQuestion;
        }
        //public static ICollection<TextQuestion> GetEntities(this ICollection<TextQuestionDTO> textQuestionsDTOs)
        //{
        //    var textQuestion = textQuestionsDTOs.Select(FromDto).ToList();

        //    return textQuestion;
        //}

    }
}
