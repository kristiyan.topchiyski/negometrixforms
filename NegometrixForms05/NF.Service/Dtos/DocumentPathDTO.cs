﻿using NF.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NF.Service.Dtos
{
    public class DocumentPathDTO
    {
        public Guid Id { get; set; }
        public string Path { get; set; }
        public Guid DocumentQuestionId { get; set; }
        public DocumentQuestion DocumentQuestion { get; set; }
    }
}
