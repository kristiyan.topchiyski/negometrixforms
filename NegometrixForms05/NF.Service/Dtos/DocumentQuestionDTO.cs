﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NF.Service.Dtos
{
    public class DocumentQuestionDTO
    {
        public Guid Id { get; set; }
        public int FileNumbers { get; set; } = 1;
        public int FileSize { get; set; } = 1;
        public bool IsRequired { get; set; }
        public ICollection<DocumentPathDTO> Paths { get; set; }
        public string DocumentPath { get; set; }
        public FormDTO Form { get; set; }
        public Guid FormId { get; set; }
        public string Name { get; set; }
    }
}
